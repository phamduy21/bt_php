<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(){
        return view('login.welcome');
    }
    public function postLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        $user = [
            'email' => $email,
            'password' => $password
        ];

        if (Auth::attempt($user)) {
            return redirect()->route('index');
        } else {
            return back();
        }
    }
    public function register()
    {
        return view('login.welcome');
    }

    public function postRegister(Request $request)
    {
        $user = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)
        ];
        $user_registered = User::create($user);
        Auth::login($user_registered);
        return redirect()->route('index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
