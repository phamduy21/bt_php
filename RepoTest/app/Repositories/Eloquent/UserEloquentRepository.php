<?php


namespace App\Repositories\Eloquent;


use App\Models\User;
use App\Repositories\BaseEloquentRepository;
use Illuminate\Support\Facades\DB;

class UserEloquentRepository extends BaseEloquentRepository
{

    public function model()
    {
        return User::class;
    }

    public function searchByName($keyword)
    {
//        1. Query Builder
//        return DB::table('users')->where('name', 'LIKE', '%' . $keyword . '%')->get();
//        2. Eloquent
        return User::where('name', 'LIKE', '%' . $keyword . '%')->get();
    }
}
