<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
    <title>User management</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />


    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext"
          rel="stylesheet">
    <!-- //web-fonts -->
</head>

<body>
@include('header')
    <h2 class="text-center mt-3">User Management</h2>
    <div class="mb-2 container">
        <a  href="{{route('user.create')}}" class="btn btn-success">Create</a>
        <div style="float: right">
            <form class="form-inline my-2 my-lg-0" action="{{route('user.search')}}" method="GET">
                @csrf
                <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search..." aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
    <div class="main-bg" >

        <div class="container">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col" style="text-align: center">Action</th>
                </tr>
                </thead>
                <thead>
                <tbody class="col text-white">
                @if(!empty($users))
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td style="text-align: center">
                                <a class="btn btn-primary" href="{{route('user.edit', $user->id)}}">Edit</a>
                                <a class="btn btn-danger" onclick="return confirm('Are You Sure!?')"
                                   href="{{route('user.delete', $user->id)}}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                </thead>
                {{--        <tbody>--}}
                {{--        @if(!empty($users))--}}
                {{--            @foreach($users as $user)--}}
                {{--                <tr>--}}
                {{--                    <th scope="row">{{$user->id}}</th>--}}
                {{--                    <td>{{$user->name}}</td>--}}
                {{--                    <td>{{$user->email}}</td>--}}
                {{--                    <td>--}}
                {{--                        <a class="btn btn-primary" href="{{route('user.edit', $user->id)}}">Edit</a> |--}}
                {{--                        <a class="btn btn-danger" onclick="return confirm('Are You Sure!?')"--}}
                {{--                           href="{{route('user.delete', $user->id)}}">Delete</a>--}}
                {{--                    </td>--}}
                {{--                </tr>--}}
                {{--            @endforeach--}}
                {{--        @endif--}}
                {{--        </tbody>--}}
            </table>
        </div>

    </div>

</body>

</html>
