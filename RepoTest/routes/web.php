<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware(['auth']) ->group(function (){
    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::get('create', [UserController::class, 'create'])->name('user.create');
    Route::post('create', [UserController::class, 'store']);

    Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::post('edit/{id}', [UserController::class, 'update']);

    Route::get('delete/{id}', [UserController::class, 'destroy'])->name('user.delete');

    Route::get('search', [UserController::class, 'search'])->name('user.search');
});

Route::get('register', [LoginController::class, 'register'])->name('register');
Route::post('register', [LoginController::class, 'postRegister']);

Route::get('login', [LoginController::class, 'login'])->name('login');
Route::post('login', [LoginController::class, 'postLogin']);

Route::get('logout', [LoginController::class, 'logout'])->name('logout');
