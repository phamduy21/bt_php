<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Calculator</title>
</head>
<body>

	<form method="GET">

		Số a:   <input type="number" name="n1">
		<br>
		Số b:   <input type="number" name="n2">
		<br>
		<br>
		<input type="submit" value="+" name="submit">
		
		<input type="submit" value="-" name="submit">
		
		<input type="submit" value="*" name="submit">
		
		<input type="submit" value="/" name="submit">
	</form>
	<?php
		function tinh($a,$b,$k){
			if($k == '+'){
				return $a+$b;
			}else if($k == '-'){
				return $a-$b;
			}else if($k == '*'){
				return $a*$b;
			}else if($k == '/'){				
					return $a/$b;
			}
		}
		if (isset($_GET['submit'])) {
			# code...
			$a = $_GET['n1'];
			$b = $_GET['n2'];
			$k = $_GET['submit'];
			if($a == '' || $b ==''){
				echo "Vui lòng nhập đầy đủ a và b";
			}else{
				if($k == '+'){
					echo "Kết quả phép tính cộng của ".$a." va ".$b." : ".tinh($a,$b,$k);
				}else if($k == '-'){
					echo "Kết quả phép tính trừ của ".$a." va ".$b." : ".tinh($a,$b,$k);
				}else if($k == '*'){
					echo "Kết quả phép tính nhân của ".$a." va ".$b." : ".tinh($a,$b,$k);
				}else if($k == '/'){
					if($b == 0){
						echo "Số b phải khác 0";
					}else{
						echo "Kết quả phép tính chia của ".$a." va ".$b." : ".tinh($a,$b,$k);
					}
				}
			}
		}
	?>
</body>
</html>