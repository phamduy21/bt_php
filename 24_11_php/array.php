<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Mảng</title>
</head>
<body>
	<form method="POST">
		<p>Mảng 1: </p>
		Nhập phần tử thứ 1 của mảng 1: <input type="number" name="a1">
		<br />
		Nhập phần tử thứ 2 của mảng 1: <input type="number" name="a2">
		<br />
		Nhập phần tử thứ 3 của mảng 1: <input type="number" name="a3">
		<br />
		Nhập phần tử thứ 4 của mảng 1: <input type="number" name="a4">
		<br />
		Nhập phần tử thứ 5 của mảng 1: <input type="number" name="a5">
		<br />
		<br />
		<p>Mảng 2: </p>
		Nhập phần tử thứ 1 của mảng 2: <input type="number" name="b1">
		<br />
		Nhập phần tử thứ 2 của mảng 2: <input type="number" name="b2">
		<br />
		Nhập phần tử thứ 3 của mảng 2: <input type="number" name="b3">
		<br />
		Nhập phần tử thứ 4 của mảng 2: <input type="number" name="b4">
		<br />
		Nhập phần tử thứ 5 của mảng 2: <input type="number" name="b5">
		<br />
		<br />
		<input type="submit" value="Tạo mảng" name="tm">
	</form>
	<br />
	<br />
	<form method="POST">
		Nhập phần tử muốn chuyển: <input type="number" name="pt">
		<select name="option">
			<option value="1">Chuyển từ mảng 1 sang mảng 2</option>
			<option value="2">Chuyển từ mảng 2 sang mảng 1</option>
		</select>
		<br />
		<br />
		<input type="submit" value="Chuyển" name="chuyen">
	</form>
	<?php
		if(isset($_POST['tm'])){
			$a1 = $_POST['a1'] ;
			$a2 = $_POST['a2'] ;
			$a3 = $_POST['a3'] ;
			$a4 = $_POST['a4'] ;
			$a5 = $_POST['a5'] ;
			$b1 = $_POST['b1'] ;
			$b2 = $_POST['b2'] ;
			$b3 = $_POST['b3'] ;
			$b4 = $_POST['b4'] ;
			$b5 = $_POST['b5'] ;
			$array1 = array($a1,$a2,$a3,$a4,$a5);
			$_SESSION['array1'] = $array1;
			$array2 = array($b1,$b2,$b3,$b4,$b5);
			$_SESSION['array2'] = $array2;
			echo "<br /><br /><br />Mảng 1: ";
			for ($i=0; $i < 5; $i++) { 
				# code...
				echo $array1[$i]." " ;
				if($i<4){
					echo ", ";
				}
			}
			echo "<br /><br />Mảng 2: ";
			for ($i=0; $i < 5; $i++) { 
				# code...
				echo $array2[$i]." " ;
				if($i<4){
					echo ", ";
				}
			}
		}
		if (isset($_POST['chuyen'])) {
			# code...
			$pt = isset($_POST['pt'])?$_POST['pt']:' ';
			$option = isset($_POST['option'])?$_POST['option']:' ';
			$array1= isset($_SESSION['array1'])?$_SESSION['array1']:null;
 			$array2= isset($_SESSION['array2'])?$_SESSION['array2']:null;
 			if(isset($array1)&&isset($array2)){
 				echo "Mảng ban đầu: ";
 				echo "<br /><br />Mảng 1: ";
				foreach ($array1 as $key1) {
					echo $key1." ";
				}
				echo "<br /><br />Mảng 2: ";
				foreach ($array2 as $key2) {
					echo $key2." ";
				}
				
 			}
 			if (isset($option)) {
 				# code...
 				switch ($option) {
 					case '1':

 						if(in_array($pt, $array1)){
 							unset($array1[array_search($pt, $array1)]);
 							array_push($array2, $pt);
 							sort($array2);
 							echo "<br /><br />Sau khi chuyển: ";
 							echo "<br /><br />Mảng 1: ";
 							foreach ($array1 as $key1) {
 								# code...
 								echo $key1." ";
 							}
 							echo "<br /><br />Mảng 2: ";
 							foreach ($array2 as $key2) {
 								# code...
 								echo $key2." ";
 							}
 						}else{
 							echo "<br />";
 							echo $pt." không nằm trong mảng 1";
 						}
 						break;
 					
 					case '2':
 						if(in_array($pt, $array2, TRUE)){
 							unset($array2[array_search($pt, $array2)]);
 							array_push($array1, $pt);
 							rsort($array1);
 							echo "<br /><br />Sau khi chuyển: ";
 							echo "<br /><br />Mảng 1: ";
 							foreach ($array1 as $key1) {
 								# code...
 								echo $key1." ";
 							}
 							echo "<br /><br />Mảng 2: ";
 							foreach ($array2 as $key2) {
 								# code...
 								echo $key2." ";
 							}
 						}else{
 							echo "<br />";
 							echo $pt." không nằm trong mảng 2";
 						}
 						break;
 				}
 									
 			}
 			
		}
	?>
</body>
</html>