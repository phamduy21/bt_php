<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\login\LoginController;
use App\Http\Controllers\user\UserController;
use App\Http\Controllers\category\CategoryController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\post\PostController;
use App\Http\Controllers\client\ClientController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth']) ->group(function (){


//    Route::get('search', [UserController::class, 'search'])->name('user.search');

    //post
//    Route::get('admin/post', [PostController::class, 'index'])->name('upload');
//    Route::post('admin/post', [PostController::class, 'store']);


    //admin
    Route::get('admin', [AdminController::class, 'admin'])->name('admin.index');
    Route::get('admin/category', [CategoryController::class, 'index'])->name('admin.category');
    Route::get('admin/user', [UserController::class, 'index'])->name('admin.user');
    Route::get('admin/post', [PostController::class, 'create'])->name('admin.post');
    Route::post('admin/post', [PostController::class, 'store']);
    Route::get('admin/tables', [PostController::class, 'index'])->name('admin.table');
    //category
    Route::get('admin/category/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('admin/category/create', [CategoryController::class, 'store']);
    Route::get('admin/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::post('admin/category/edit/{id}', [CategoryController::class, 'update']);
    Route::get('admin/category/delete/{id}', [CategoryController::class, 'destroy'])->name('category.delete');


    //user
    Route::get('admin/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('admin/user/create', [UserController::class, 'store']);
    Route::get('admin/user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::post('admin/user/edit/{id}', [UserController::class, 'update']);
    Route::get('admin/user/delete/{id}', [UserController::class, 'destroy'])->name('user.delete');
    //post
    Route::post('admin/post/create', [PostController::class, 'store']);
    Route::get('admin/post/edit/{id}', [PostController::class, 'edit'])->name('post.edit');
    Route::post('admin/post/edit/{id}', [PostController::class, 'update']);
    Route::get('admin/post/delete/{id}', [PostController::class, 'destroy'])->name('post.delete');
});

Route::get('register', [LoginController::class, 'register'])->name('register');
Route::post('register', [LoginController::class, 'postRegister']);

Route::get('login', [LoginController::class, 'login'])->name('login');
Route::post('login', [LoginController::class, 'postLogin']);

Route::get('logout', [LoginController::class, 'logout'])->name('logout');

//client
Route::get('elftech', [ClientController::class, 'index'])->name('client.index');
Route::get('', [ClientController::class, 'index']);
Route::get('elftech/blog', [ClientController::class, 'blog'])->name('client.blog');
Route::get('elftech/post_details/{id}', [ClientController::class, 'post_details'])->name('client.post_details');

