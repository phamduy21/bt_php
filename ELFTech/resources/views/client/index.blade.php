<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

    <title>ELF Tech</title>

    <!-- Bootstrap core CSS -->
    <link href="v/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-stand-blog.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <!--

    TemplateMo 551 Stand Blog

    https://templatemo.com/tm-551-stand-blog

    -->
</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->

<!-- Header -->
<header class="">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('client.index')}}">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-mobile-alt"></i>
                </div>
                <div class="sidebar-brand-text mx-3">
                    <b>ELF TECH</b>
                    <sup>⚡</sup>
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('client.index')}}">Trang chủ
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('client.blog')}}">Tin mới</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{route('client.blog')}}">Chuyên mục</a>
                        <div class="dropdown-menu">
                            @if(!empty($categories))
                                @foreach($categories as $category)
                                    <a class="dropdown-item" href="">{{$category -> nameCategory}}</a>
                                @endforeach
                            @endif
                        </div>
                    </li>

                    <div class="mr-5" style="float: right">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                @if(auth()->user())
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{strtoupper(auth()->user()->name)}}
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{route('logout')}}">Đăng xuất</a>
                                    </div>
                                @else
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Đăng nhập
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{route('login')}}">Đăng nhập</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{route('register')}}">Đăng ký</a>
                                    </div>
                                @endif
                            </li>
                        </ul>
                    </div>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- Page Content -->
<!-- Banner Starts Here -->
<div class="main-banner header-text">
    <div class="container-fluid">
        <div class="owl-banner owl-carousel">
            @if(!empty($posts))
                @foreach($posts as $post)
                    <div class="item">
                        <img style="width: 436px;height: 378px " src="images/{{$post->nameImage}}" alt="">
                        <div class="item-content">
                            <div class="main-content">
                                <div class="meta-category">
                                    <span>{{$post->category}}</span>
                                </div>
                                <a href="{{ route('client.post_details',$post -> id) }}"><h4>{{$post->title}}</h4></a>
                                <ul class="post-info">
                                    <li><a>Admin</a></li>
                                    <li><a>{{$post->created_at}}</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>



<section class="blog-posts">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="all-blog-posts">
                    <div class="row">
                        @if(!empty($posts))
                            @foreach($posts as $post)
                                <div class="col-lg-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img style="width: 730px;height: 322.33px " src="images/{{$post->nameImage}}" alt="">
                                        </div>
                                        <div class="down-content">
                                            <span>{{$post->category}}</span>
                                            <a href="{{ route('client.post_details',$post -> id) }}"><h4>{{$post->title}}</h4></a>
                                            <ul class="post-info">
                                                <li><a href="#">Admin</a></li>
                                                <li><a>{{$post->created_at}}</a></li>
                                            </ul>
                                            <p>{{$post->subtitle}}</p>
                                            <div class="post-options">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <ul class="post-tags">
                                                            <li><i class="fa fa-tags"></i></li>
                                                            <li><a href="#">{{$post->category}}</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sidebar-item search">
                                <form id="search_form" name="gs" method="GET" action="#">
                                    <input type="text" name="q" class="searchText" placeholder="Tìm kiếm ..." autocomplete="on">
                                </form>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="sidebar-item tags">
                                <div class="sidebar-heading">
                                    <h2>Chuyên mục</h2>
                                </div>
                                <div class="content">
                                    <ul>
                                        @if(!empty($categories))
                                            @foreach($categories as $category)
                                                <li><a href="#">{{$category -> nameCategory}}</a></li>
                                            @endforeach
                                        @endif

                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="social-icons">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Behance</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Dribbble</a></li>
                </ul>
            </div>
            <div class="col-lg-12">
                <div class="copyright-text">
                    <p>Copyright 2020 ELF Tech.

                        | Design: <a rel="nofollow" href="http://localhost:8000/elftech" target="_parent">ELF</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="v/jquery/jquery.min.js"></script>
<script src="v/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Additional Scripts -->
<script src="assets/js/custom.js"></script>
<script src="assets/js/owl.js"></script>
<script src="assets/js/slick.js"></script>
<script src="assets/js/isotope.js"></script>
<script src="assets/js/accordions.js"></script>

<script language = "text/Javascript">
    cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
    function clearField(t){                   //declaring the array outside of the
        if(! cleared[t.id]){                      // function makes it static and global
            cleared[t.id] = 1;  // you could use true and false, but that's more typing
            t.value='';         // with more chance of typos
            t.style.color='#fff';
        }
    }
</script>

</body>
</html>
