<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
    <title>Login</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Triple Forms Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Meta tag Keywords -->

    <!-- css files -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext"
          rel="stylesheet">
    <!-- //web-fonts -->
</head>

<body>
<div class="main-bg">
    <!-- title -->
    <h1>Login</h1>
    <!-- //title -->
    <div class="sub-main-w3">
        <div class="image-style">

        </div>
        <!-- vertical tabs -->
        <div class="vertical-tab">
            <div id="section1"  class="section-w3ls">
                <input type="radio" name="sections" id="option1" checked>
                <label for="option1" class="icon-left-w3pvt"><span class="fa fa-user-circle" aria-hidden="true"></span>Login</label>
                <article>
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <h3 class="legend">Login Here</h3>
                        <div class="input">
                            <span class="fa fa-envelope-o" aria-hidden="true"></span>
                            <input type="email" placeholder="Email" name="email" required />
                        </div>
                        <div class="input">
                            <span class="fa fa-key" aria-hidden="true"></span>
                            <input type="password" placeholder="Password" name="password" required />
                        </div>
                        <button type="submit" class="btn submit">Login</button>
                        <a href="#" class="bottom-text-w3ls">Register?</a>
                    </form>
                </article>
            </div>
            <div id="section2"  class="section-w3ls">
                <input type="radio" name="sections" id="option2">
                <label for="option2" class="icon-left-w3pvt"><span class="fa fa-pencil-square" aria-hidden="true"></span>Register</label>
                <article>
                    <form action="{{route('register')}}" method="post">
                        @csrf
{{--                        <h3 class="legend">Register Here</h3>--}}
                        <div class="input">
                            <span class="fa fa-user-o" aria-hidden="true"></span>
                            <input type="text" placeholder="Username" name="name" pattern="[\s\p{L}]{3,30}" required />
                        </div>
                        <div class="input">
                            <span class="fa fa-envelope-o" aria-hidden="true"></span>
                            <input type="email" placeholder="Email" name="email" pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$"
                                   maxlength="27" required />
                        </div>
                        <div class="input">
                            <span class="fa fa-envelope-o" aria-hidden="true"></span>
                            <input type="number" placeholder="Phone number" name="phone" pattern="0\d{9}" required />
                        </div>
                        <div class="input">
                            <span class="fa fa-key" aria-hidden="true"></span>
                            <input type="password" placeholder="Password" name="password" required />
                        </div>
                        <button type="submit" class="btn submit">Register</button>
                    </form>
                </article>
            </div>
        </div>
        <!-- //vertical tabs -->
        <div class="clear"></div>
    </div>

</div>

</body>

</html>
