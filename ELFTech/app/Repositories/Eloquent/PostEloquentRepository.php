<?php


namespace App\Repositories\Eloquent;


use App\Models\Post;
use App\Repositories\BaseEloquentRepository;

class PostEloquentRepository extends BaseEloquentRepository
{

    public function model()
    {
        // TODO: Implement model() method.
        return Post::class;
    }

    public function searchByName($keyword)
    {
        // TODO: Implement searchByName() method.
    }
}
