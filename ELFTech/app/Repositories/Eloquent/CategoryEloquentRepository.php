<?php


namespace App\Repositories\Eloquent;


use App\Models\Category;
use App\Repositories\BaseEloquentRepository;

class CategoryEloquentRepository extends BaseEloquentRepository
{

    public function model()
    {
        // TODO: Implement model() method.
        return Category::class;
    }

    public function searchByName($keyword)
    {
        // TODO: Implement searchByName() method.
        return Category::where('nameCategory', 'LIKE', '%' . $keyword . '%')->get();
    }
}
