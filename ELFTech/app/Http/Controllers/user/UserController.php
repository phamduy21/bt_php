<?php

namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\UserEloquentRepository;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    protected $userRepository;

    /**
     * UserController constructor.
     * @param $userRepository
     */
    public function __construct(UserEloquentRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function index()
    {
        $users = $this->userRepository->all();
        return view('admin.user', [
            'users' => $users
        ]);
    }
    public function create()
    {
        return view('user.create_user');
    }

    public function store(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $password = $request->get('password');

        $user = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'password' => Hash::make($password)
        ];

        $this->userRepository->create($user);
        return redirect()->route('admin.user');
    }

    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('user.edit_user', [
            'user' => $user
        ]);

    }

    public function update(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        $userId = $user->id;

        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');

        $data = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ];

        $this->userRepository->update($data, $userId);
        return redirect()->route('admin.user');
    }

    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        $userId = $user->id;

        $this->userRepository->delete($userId);
        return redirect()->route('admin.user');
    }

    public function search(Request $request)
    {
        $keyword = $request->get('keyword');

//        1. Use function in Repo
//        $users = $this->userRepository->searchByName($keyword);
//        2. Use custom query
        $users = $this->userRepository->customQuery()->where('name', 'LIKE', '%' . $keyword . '%')
            -> orWhere('email', 'LIKE', '%' . $keyword . '%')
            -> orWhere('phone', 'LIKE', '%' . $keyword . '%')
            ->get();

        return view('admin.user', [
            'users' => $users
        ]);
    }

}
