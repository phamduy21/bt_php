<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryEloquentRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('admin.category', [
            'categories' => $categories
        ]);
    }
    public function create()
    {
        $categories = $this->categoryRepository->all();
        return view('category.create_category', [
            'categories' => $categories
        ]);
    }

    public function store(Request $request)
    {
        $nameCategory = $request->get('nameCategory');

        $category = [
            'nameCategory' => $nameCategory
        ];

        $this->categoryRepository->create($category);
        return redirect()->route('admin.category');
    }

    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);
        return view('category.edit_category', [
            'category' => $category
        ]);

    }

    public function update(Request $request, $id)
    {
        $category= $this->categoryRepository->find($id);
        $categoryId = $category->id;

        $nameCategory = $request->get('nameCategory');

        $data = [
            'nameCategory' => $nameCategory
        ];

        $this->categoryRepository->update($data, $categoryId);
        return redirect()->route('admin.category');
    }

    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);
        $categoryId = $category->id;

        $this->categoryRepository->delete($categoryId);
        return redirect()->route('admin.category');
    }
}
