<?php

namespace App\Http\Controllers\post;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\PostEloquentRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postRepository;
    protected $categoryRepository;
    /**
     * PostController constructor.
     * @param $postRepository
     */
    public function __construct( PostEloquentRepository $postRepository, CategoryEloquentRepository $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function index(){
        $categories = $this->categoryRepository->all();
        $posts = $this -> postRepository -> all();
        return view('admin.tables',[
            'posts' => $posts, 'categories' => $categories

        ]);

    }
    public function create(){
        $categories = $this->categoryRepository->all();

        return view('admin.post', [
            'categories' => $categories
        ]);
    }
    public function store(Request $request){
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $contentblog = $request->get('contentblog');
        $category = $request->get('category');
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $nameImage = time().'.'.$request->image->extension();

        $request->image->move(public_path('images'), $nameImage);
        $post = [
            'title' => $title,
            'subtitle' => $subtitle,
            'contentblog' => $contentblog,
            'category' => $category,
            'nameImage' => $nameImage
        ];
        $this -> postRepository -> create($post);
        return  redirect()-> route('admin.table');
    }
    public function edit($id)
    {
        $post = $this->postRepository->find($id);
        $categories = $this->categoryRepository->all();
        return view('post.edit_post',
            ['post' => $post, 'categories' => $categories]
        );

    }
    public function update(Request $request, $id){
        $post = $this->postRepository->find($id);
        $postId = $post->id;
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $contentblog = $request->get('contentblog');
        $category = $request->get('category');
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $nameImage = time().'.'.$request->image->extension();

        $request->image->move(public_path('images'), $nameImage);
        $post = [
            'title' => $title,
            'subtitle' => $subtitle,
            'contentblog' => $contentblog,
            'category' => $category,
            'nameImage' => $nameImage
        ];
        $this -> postRepository -> update($post,$postId);
        return  redirect()-> route('admin.table');
    }
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);
        $postId = $post->id;

        $this->postRepository->delete($postId);
        return redirect()->route('admin.table');
    }

}
