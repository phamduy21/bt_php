<?php

namespace App\Http\Controllers\login;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(){
        return view('login.welcome');
    }
    public function postLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        $user = [
            'email' => $email,
            'password' => $password
        ];

        if (Auth::attempt($user)) {
            if($email=='phamduy2479@gmail.com'){
                return redirect()->route('admin.index');
            }else{
                return redirect()->route('client.index');
            }
        } else {
            return back();
        }
    }
    public function register()
    {
        return view('login.welcome');
    }

    public function postRegister(Request $request)
    {
        $user = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)
        ];
        $user_registered = User::create($user);
        Auth::login($user_registered);
        return redirect()->route('client.index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
