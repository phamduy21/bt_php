<?php

namespace App\Http\Controllers\client;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\PostEloquentRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    protected $postRepository;
    protected $categoryRepository;

    /**
     * ClientController constructor.
     * @param $postRepository
     * @param $categoryRepository
     */
    public function __construct( PostEloquentRepository $postRepository, CategoryEloquentRepository $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
    }


    public function index(){
        $posts = $this -> postRepository -> all();
        $categories = $this->categoryRepository->all();
        return view('client.index',[
            'posts' => $posts, 'categories' => $categories
        ]);
    }
    public function post_details($id){
        $post = $this->postRepository->find($id);
        $categories = $this->categoryRepository->all();
        return view('client.post_details',
            ['post' => $post, 'categories' => $categories]
        );
    }
    public function blog(){
        $posts = $this -> postRepository -> all();
        $categories = $this->categoryRepository->all();
        return view('client.blog',[
            'posts' => $posts, 'categories' => $categories
        ]);
    }
}
