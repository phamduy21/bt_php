<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryEloquentRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function admin(){
        $categories = $this->categoryRepository->all();
        return view('admin.admin', [
            'categories' => $categories
        ]);
    }
    public function category(){
        $categories = $this->categoryRepository->all();
        return view('admin.category', [
            'categories' => $categories
        ]);
    }
    public function post(){
        $categories = $this->categoryRepository->all();
        return view('admin.post', [
            'categories' => $categories
        ]);
    }
    public function tables(){
        $categories = $this->categoryRepository->all();
        return view('admin.tables', [
            'categories' => $categories
        ]);
    }
    public function user(){
        $categories = $this->categoryRepository->all();
        return view('admin.user', [
            'categories' => $categories
        ]);
    }
}
