<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;
    protected $fillable = [
        'title','subtitle','contentblog','nameImage','category'
    ];
}
