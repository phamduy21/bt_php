<?php session_start(); ?>
<?php
    require_once "customer.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Customer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
</head>
<body>
    <div class="container">
        <form method="POST" action="">
            <h4 class="text-center" style="padding:30px;">Add Customer</h4><br>
            <div class="form-group">
                <label for="id">ID:</label>
                <input type="text" class="form-control" name="id" placeholder="Enter id" required="">
            </div>
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" placeholder="Enter name" required="">
            </div>
            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" name="phone" placeholder="Enter phone" required="">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" placeholder="Enter email" required="">
            </div>
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address" placeholder="Enter address" required="">
            </div>
            <input type="submit" name="submit" class="btn btn-primary" style="float:right;" value="Submit">
        </form>
        <a href="./homepage.php" class="btn btn-danger">Back</a>
    </div>
    <?php
        if (isset($_POST['submit'])){

            $id = isset($_POST['id'])?$_POST['id']:'';
            $name = isset($_POST['name'])?$_POST['name']:'';
            $phone = isset($_POST['phone'])?$_POST['phone']:'';
            $email = isset($_POST['email'])?$_POST['email']:'';
            $address = isset($_POST['address'])?$_POST['address']:'';
            $customer = new Customer($id, $name, $phone, $email, $address);
            $customers = isset($_SESSION['customers']) ? $_SESSION['customers'] : '';
            if(isset($customers)){

                array_push($customers,$customer);
                $_SESSION['customers'] = $customers;

            }else{
                $customers = array();
                array_push($customers,$customer);
                $_SESSION['customers'] = $customers;
            }
            header("Location: http://localhost/management/homepage.php");
        }

    ?>
</body>
</html>