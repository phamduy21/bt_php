<?php
    require_once 'customer.php';
    session_start();
    //onclick="confirm('Are you sure want to delete this customer')"
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
</head>
<body>
    <div class="container menu-nav">
        <h4 class="text-center" style="padding:30px;">Customer Information</h4><br>
        <a href="addCustomer.php" class="btn btn-primary" style="float:right;" >Add Customer</a><br><br>
        <table class="table table-hover" >
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Modify</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    $customers = isset($_SESSION['customers']) ? $_SESSION['customers'] : ' ';
                    if(isset($_SESSION['customers'])){
                        foreach ($customers as $c){
                ?>
                <tr>
                    <td><?php
                            echo $c-> getId();
                        ?></td>
                    <td><?php
                            echo $c -> getName();
                        ?></td>
                    <td><?php
                            echo $c -> getPhone();

                        ?></td>
                    <td><?php
                            echo $c -> getEmail();

                        ?></td>
                    <td><?php
                            echo $c -> getAddress();

                        ?></td>
                    <td>
                        <button type="submit" style="color:green" name="edit" >
                            <i class="fa fa-pencil" aria-hidden="true"></i></button> &nbsp; &nbsp; &nbsp;

                        <form method="post">
                            <button type="submit" name="delete" style="color:red" >
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                            <input type="hidden" name="dlt" value="<?php
                            echo $c -> getId();?>">

                        </form>
                    </td>
                </tr>
                <?php }
                }?>

            </tbody>
        </table>
    </div>
    <?php
        $dlt = isset($_POST['dlt'])? $_POST['dlt'] : '';

        for ($i =0; $i< count($customers); $i++){

            if(($customers[$i] -> getId()) == $dlt){
                array_splice($customers,$i,1);
            }
        }
        $_SESSION['customers'] = $customers;
    ?>
</body>
</html>